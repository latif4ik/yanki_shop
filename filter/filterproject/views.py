from django.shortcuts import render
from .models import *
from rest_framework import generics
from .serializers import *

def index(request):
    products = Products.objects.all()
    if request.method == 'POST':
        brand_list = list(map(int, request.POST.getlist('category')))
        if not len(brand_list):
            products = Products.objects.all()
        elif len(brand_list) == 1:
            products = Products.objects.filter(brand_id=brand_list[0])
        else:
            products = Products.objects.filter(brand_id__in=brand_list)
    brands = Brands.objects.all()
    context = {
        'title': 'main page',
        'brands': brands,
        'products': products
    }
    return render(request, 'filter/index.html', context)


class ProductsListAPIView(generics.ListAPIView):
    serializer_class = ProductSerializer
    def get_queryset(self):
        try:
            return Products.objects.filter(brand_id=self.kwargs['cat_id'])

        except:
            return Products.objects.all()


class BrandListAPIView(generics.ListAPIView):
    queryset = Brands.objects.all()
    serializer_class = CategorySerializer


class FavouritesCreateListAPIView(generics.ListCreateAPIView):
    queryset = Favourite.objects.all()
    serializer_class = FavouriteSerializer

class RemoveFromFavouritesAPIView(generics.DestroyAPIView):
    queryset = Favourite.objects.all()
    serializer_class = FavouriteSerializer