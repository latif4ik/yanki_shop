from rest_framework import serializers
from .models import *


class ProductSerializer(serializers.ModelSerializer):
    size = serializers.StringRelatedField(many=True)
    color = serializers.StringRelatedField(many=True)
    brand = serializers.StringRelatedField()

    class Meta:
        model = Products
        fields = '__all__'
        read_only = "__all__"


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Brands
        fields = '__all__'


class FavouriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Favourite
        fields = '__all__'

    def create(self, validated_data):
        if Products.objects.get(id=validated_data['product']) in Favourite.objects.all():
            return

    def to_representation(self, instance):
        self.fields['product'] = ProductSerializer(read_only=True)
        return super(FavouriteSerializer, self).to_representation(instance)
