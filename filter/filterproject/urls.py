from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from .views import *


urlpatterns = [
    path('', index, name='home'),
    path('yanki/api/v1/pro_list/', ProductsListAPIView.as_view()),
    path('yanki/api/v1/pro_list/<int:cat_id>', ProductsListAPIView.as_view()),
    path('yanki/api/v1/cat_list', BrandListAPIView.as_view()),
    path('yanki/api/v1/wish_list', FavouritesCreateListAPIView.as_view()),
    path('yanki/api/v1/wish_list/<int:pk>', RemoveFromFavouritesAPIView.as_view()),
]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
