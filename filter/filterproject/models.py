from django.db import models


class Brands(models.Model):
    title = models.CharField(max_length=255, verbose_name='название бренда')
    image = models.ImageField(upload_to='brand')

    def __str__(self):
        return self.title


class Products(models.Model):
    title = models.CharField(max_length=255, verbose_name='название продукта')
    brand = models.ForeignKey(Brands, on_delete=models.CASCADE, verbose_name='бренд')
    image = models.ImageField(upload_to='products', verbose_name='фото продукта')
    price = models.IntegerField(verbose_name='цена')
    size = models.ManyToManyField('Size', verbose_name='размер')
    color = models.ManyToManyField('Color', verbose_name='цвет')
    def __str__(self):
        return self.title


class Size(models.Model):
    size = models.CharField(max_length=255, verbose_name='размер',)
    def __str__(self):
        return self.size
class Color(models.Model):
    color = models.CharField(max_length=255, verbose_name='цвет')
    def __str__(self):
        return self.color

class Favourite(models.Model):
    product = models.ForeignKey(Products,on_delete=models.CASCADE,related_name='product')